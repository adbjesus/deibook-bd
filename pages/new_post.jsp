<jsp:include page="header.jsp"/>
<!--<h1 class="notify">To start navigation use the upper right menu!</h1>-->
<jsp:include page="sidebar_posts.jsp"/>
<div id="content" style="min-height: 200px">
	<form action="/bd/AddPost" method="post" enctype="multipart/form-data">
		<input name="post_title" type="text" placeholder="Title"/>
		<input name="post_image" type="file" style="border: none; padding: 0"/>
		<textarea name="post_text" placeholder="Post here" style="resize: none; width: 100%; height:150px; padding: 3px"></textarea>
		<input class="button left" type="submit" value="Post"/>
	</form>
</div>
<jsp:include page="footer.jsp"/>

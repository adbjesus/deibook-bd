<jsp:include page="header.jsp"/>
	<%@page import="bean.Functions"%>
	<%@page import="java.util.ArrayList"%>
	<jsp:include page="sidebar_mensagens.jsp"/>

	<div id="content">
		<ul id="zebra_table">
			<ul id="row" class="title">
				<li id="elem1"> Sender </li>
				<li id="elem2"> Body </li>
				<li id="elem3"> Date </li>
			</ul>
			<%
			Functions f = (Functions) session.getAttribute("functions"); 
			ArrayList<String[]> msgs = f.getInbox();
			if(msgs!=null){
			String[] msg;
			String ev_od = "even";
			for (int i=0;i<msgs.size();i++){
				if(i%2 == 0){
					ev_od = "even";
				}
				else{
					ev_od = "odd";
				}
				msg = msgs.get(i);
				out.println("<ul id='row' class='" + ev_od+ "'><a href='/bd/pages/view_messages.jsp?msgID="
				+ Integer.toString(i) + "'><li id='elem1'>"
				+ msg[1] + "</li><li id='elem2'>"
				+ msg[3] + "</li><li style='font-size: 14px' id='elem3'>"
				+ msg[4] 
				+ "</li></a></ul>" );
			}}
		%>
	</ul>
</div>

<jsp:include page="footer.jsp"/>

<jsp:useBean id="functions" class="bean.Functions" scope="session"/>
<jsp:setProperty name="functions" property="*"/>
<%@page import="bean.Functions"%>
<%
Functions f = (Functions)session.getAttribute("functions");
if(f==null || f.getUsername()==null){
	pageContext.forward("index.jsp");	
}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/bd/css/style.css" type="text/css">
</head>
<body>
<div id="container">
	<div id="header" class="grey">
		<div class="centered foot_head">
			<div id="logo" class="left"><a href="/bd/pages/main.jsp">DeiBook</a></div>
			<div id="header_list" class="right">
				<a href="/bd/pages/chatrooms.jsp">Chatrooms</a>
				<a href="/bd/pages/mensagens.jsp">Messages</a>
				<a href="/bd/pages/profile.jsp">${functions.username}</a>
				<a href="/bd/Logout">Logout</a>
			</div>
		</div>
	</div>
	<div id="main" class="centered lowered">
		
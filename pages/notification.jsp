<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="refresh" content="3; url=/bd/pages/index.jsp"> 
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/bd/css/style.css" type="text/css">
	<jsp:useBean id="functions" class="bean.Functions" scope="session"/>
</head>
<body>
<div id="container">
	<div id="header">
		<div class="centered foot_head grey">
			<div id="logo" class="grey">DeiBook</div>
		</div>
	</div>
	
	<div id="main" class="lowered centered">
		<% String msg = (String) session.getAttribute("msg");%>
		<h1 class="notify"><%=msg%><h1>
<jsp:include page="footer.jsp"/>

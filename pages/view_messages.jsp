<jsp:include page="header.jsp"/>
<jsp:include page="sidebar_mensagens.jsp"/>

<div id="content">
	<h1 id="mensagem_noti">Mensagem</h1>
	<div id="msg_read_from">
		<b>From:</b> ${functions.msgSender}
	</div>
	<div id="msg_read_from">
		<b>To:</b> ${functions.msgReceiver}
	</div>
	<div id="msg_read_from">
		<b>Send Date:</b> ${functions.msgSendDate}
	</div>

	<div id="msg_read_from">
		<b>Read Date:</b> ${functions.msgReadDate}
	</div>
	<div id = "msg_read_body">
		<b>Body:</b> 
		<p id= "msg_txt">${functions.msgBody}</p>
	</div>
	
	<form method="get" action="/bd/pages/mensagens.jsp">
		<input class="button send" type="submit" value="Back"/>
	</form>
	<%@page import="bean.Functions"%>
	<% 
		Functions f = (Functions) session.getAttribute("functions");
		String link = "/bd/DeleteMessage?msg="+f.getMsgRealID();
		if(f.getMsgReadDate()==null){
			out.println("<a href='"+link+"'>Delete message</a>");
		}
		%>
</div>
<jsp:include page="footer.jsp"/>

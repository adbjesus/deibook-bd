<jsp:useBean id="functions" class="bean.Functions" scope="session"/>
<jsp:setProperty name="functions" property="*"/>
<%@page import="bean.Functions"%>
<%
Functions f = (Functions)session.getAttribute("functions");
if(f!=null && f.getUsername()!=null){
	pageContext.forward("main.jsp");	
}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/bd/css/style.css" type="text/css">
</head>
<body>
	<div id="container">
		<div id="header" class="grey">
			<div class="centered foot_head">
				<div id="logo" class="left"><a href="#">DeiBook</a></div>
			</div>
		</div>
	<div id="main" class="centered lowered">

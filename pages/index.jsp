<jsp:include page="header_front.jsp"/>
	
		<div id="login_form" class="form">
			<h1>Login</h1>
			<form action="/bd/Login" method="post">
				<input name="username_field" type="text" placeholder="Username"/>
				<input name="pass_field" type="password" placeholder="Password"/>
				<input class="button" type="submit" value="Login"/>
			</form>
		</div>
		<div id="register_form" class="form">
			<h1>Register</h1>
			<form action="/bd/Register" method="post">
				<input required name="username_field" type="text" placeholder="Username"/>
				<input required name="pass_field" type="password" placeholder="Password"/>
				<input name="first_name_field" type="text" placeholder="First Name"/>
				<input name="last_name_field" type="text" placeholder="Last name"/>
				<input name="mail_field" type="text" placeholder="Mail"/>
				<input name="city_field" type="text" placeholder="City"/>
				<input name="country_field" type="text" placeholder="Country"/>
				<input name="date_field" type="date" placeholder="Birthday"/>
				<div class="radio_btn_text">Male</div>
				<input class="radio_btn" type="radio" name="gender_field" value="M" checked/>
				<div class="radio_btn_text">Female</div>
				<input class="radio_btn" type="radio" name="gender_field" value="F"/>
				<div class="radio_btn_text">Other</div>
				<input class="radio_btn" type="radio" name="gender_field" value="O"/>

				<input class="button" type="submit" value="Register"/>
			</form>
		</div>
<jsp:include page="footer.jsp"/>

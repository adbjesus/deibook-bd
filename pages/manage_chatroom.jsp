<jsp:include page="header.jsp"/>
<%@page import="bean.Functions"%>
	<jsp:include page="sidebar_account.jsp"/>
	<div id="content">
		<h1 id="mensagem_noti">Manage Chatroom</h1>
		<form action="/bd/UpdateChatroom" method="post">
			<%
			Functions f = (Functions)session.getAttribute("functions");

			String[] chatroom = f.getChatroom();

			String open_checked = "";
			String closed_checked = "";
			if(chatroom[3].equals("t")){
				open_checked = "checked";
				closed_checked = "";
			}
			else{
				open_checked = "";
				closed_checked = "checked";
			}

			String theme = chatroom[2];

			%>
			<h2>Chatroom theme</h2>
			<input name="theme_field" type="text" value="<%= theme %>" placeholder="First name"/>
			<h2>Chatroom status</h2>
			<div class="radio_btn_text">Open</div>
			<input class="radio_btn" type="radio" name="status_field" value="true" <%=open_checked%> />
			<br>
			<div class="radio_btn_text">Closed</div>
			<input class="radio_btn" type="radio" name="status_field" value="false" <%=closed_checked%> />
			<br>
			<input class="button" type="submit" value="Update"/>

		</form>
		<br>

		<form action="/bd/GivePermission" method="post">
			<input name="name_field" type="text" placeholder="Username"/>
			Post and read permission
			<input class="radio_btn" type="radio" name="post_field" value="true"/>
			<br>
			Read permission
			<input class="radio_btn" type="radio" name="post_field" value="false" checked/>
			<br>
			<input class="button" type="submit" value="Add"/>
		</form>

	</div>
<jsp:include page="footer.jsp"/>


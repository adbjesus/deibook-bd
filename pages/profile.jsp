<jsp:include page="header.jsp"/>
<%@page import="bean.Functions"%>
	<jsp:include page="sidebar_account.jsp"/>
	<div id="content">
		<h1 id="mensagem_noti">Profile of ${functions.username}</h1>
		<form action="/bd/UpdateProfile" method="post">
			<%
			Functions f = (Functions)session.getAttribute("functions");

			Boolean profileType = f.getProfileType(f.getUsername());
			String public_checked = "";
			String private_checked = "";
			if(profileType){
				public_checked = "checked";
				private_checked = "";
			}
			else{
				public_checked = "";
				private_checked = "checked";
			}

			String first_name = f.getFirstName(f.getUsername());
			String last_name = f.getLastName(f.getUsername());
			String mail = f.getMail(f.getUsername());
			String age = f.getAge(f.getUsername());
			String city = f.getCity(f.getUsername());
			String country = f.getCountry(f.getUsername());
			String date_str = f.getDate(f.getUsername());
			String gender = f.getGender(f.getUsername());

			if(first_name==null) first_name = "";
			if(last_name==null) last_name = "";
			if(mail==null) mail = "";
			if(age==null) age = "";
			if(city==null) city = "";
			if(country==null) country = "";

			java.sql.Date date = null;

			if(date_str!=null){
				date = java.sql.Date.valueOf(date_str);
			}
			System.out.println(date);

			String male = "";
			String female = "";
			String other = "";

			if(gender==null);
			else if(gender.equals("M")) male = "checked";
			else if(gender.equals("F")) female = "checked";
			else if(gender.equals("O")) other = "checked";

			%>

			<input name="first_name_field" type="text" value="<%= first_name %>" placeholder="First name"/>
			<input name="last_name_field" type="text" value="<%= last_name %>" placeholder="Last name"/>
			<input name="mail_field" type="text" value="<%= mail %>" placeholder="Mail"/>
			<input name="city_field" type="text" value="<%= city %>" placeholder="City"/>
			<input name="country_field" type="text" value="<%= country %>" placeholder="Country"/>
			<input name="date_field" type="date" value="<%= date %>" />
			<br>
			<h2>Gender</h2>
			<div class="radio_btn_text">Male</div>
			<input class="radio_btn" type="radio" name="gender_field" value="M" <%=male%> />
			<br>
			<div class="radio_btn_text">Female</div>
			<input class="radio_btn" type="radio" name="gender_field" value="F" <%=female%> />
			<br>
			<div class="radio_btn_text">Other</div>
			<input class="radio_btn" type="radio" name="gender_field" value="O" <%=other%> />
			<br>
			<h2>Profile Type</h2>
			<div class="radio_btn_text">Public</div>
			<input class="radio_btn" type="radio" name="profile_type" value="true" <%=public_checked%> />
			<br>
			<div class="radio_btn_text">Private</div>
			<input class="radio_btn" type="radio" name="profile_type" value="false" <%=private_checked%> />
			<br>
			<input class="button" type="submit" value="Update"/>
		</form>
	</div>
<jsp:include page="footer.jsp"/>


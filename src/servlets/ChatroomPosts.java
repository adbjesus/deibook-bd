package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class ChatroomPosts extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String id = request.getParameter("id");
			int id_int = -1;
			try{
				id_int = Integer.parseInt(id);
			} catch(Exception e){
				response.sendRedirect("/bd/pages/search_result_chatrooms.jsp");
				return;
			}
			f.setChatroom(id_int);
			f.readPosts(id_int);
			response.sendRedirect("/bd/pages/posts.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class SendMessage extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			Functions f = (Functions)session.getAttribute("functions");
			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String receiver = request.getParameter("dest_field");
			String body = request.getParameter("body_field");
			String date = request.getParameter("date_field");
			String hours = request.getParameter("hours_field");
			String minutes = request.getParameter("minutes_field");

			if(receiver==null || body==null || receiver.equals("") || body.equals("")){
				response.sendRedirect("pages/mensagens.jsp");	
			}

			String ts = null;
			if(date==null || date.equals(""));
			else if(hours==null || hours.equals("")){
				ts = date;
			}
			else if(minutes==null || minutes.equals("")){
				ts = date+" "+hours+":00:00";
			}
			else{
				ts = date+" "+hours+":"+minutes+":00";
			}
			f.sendMessage(receiver,body,ts);
			response.sendRedirect("pages/mensagens.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doPost(request,response);
	}
}
package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class DeleteMessage extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String msg = request.getParameter("msg");
			int msg_int;
			try{
				msg_int = Integer.parseInt(msg);
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("Delete message: false, parseint");
				response.sendRedirect("/bd/pages/mensagens.jsp");			
				return;
			}
			
			Boolean sucess = f.deleteMessage(msg_int);
			System.out.println("Delete message: "+sucess);

			response.sendRedirect("/bd/pages/mensagens.jsp");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class GivePermission extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
	 		HttpSession session = request.getSession(true);
			
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}

			String name = request.getParameter("name_field");
	 		String post = request.getParameter("post_field");
	 		Boolean post_bool = false;

	 		if(post!=null && post.equals("true")){
	 			post_bool = true;
	 		}

	 		Boolean login = f.givePermission(Integer.parseInt(f.getChatroom()[0]),name,post_bool);			
			
	 		response.sendRedirect("/bd/pages/list_chatrooms.jsp");
		} catch (Exception e) {
			e.printStackTrace();
	 	}
	}
}
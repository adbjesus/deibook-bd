package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class CreateChatroom extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
	 		HttpSession session = request.getSession(true);
			
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}

			String theme = request.getParameter("theme_field");

	 		Boolean sucess = f.createChatroom(theme);			
			
	 		System.out.println("Chatroom created: "+sucess);

	 		response.sendRedirect("/bd/pages/chatrooms.jsp");
	 		return;
		} catch (Exception e) {
			e.printStackTrace();
	 	}
	}
}
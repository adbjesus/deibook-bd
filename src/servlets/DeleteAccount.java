package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class DeleteAccount extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			
			Boolean sucess = f.deleteUser();

			if(sucess){
				session.invalidate();
				response.sendRedirect("/bd/pages/index.jsp");
			}
			else{
				response.sendRedirect("/bd/pages/settings.jsp");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
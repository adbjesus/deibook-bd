package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;


public class Register extends HttpServlet{
	private String msg;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}
			if(f.getUsername()!=null){
				f.setUsername(null);
			}

			String username = request.getParameter("username_field");
			String password = request.getParameter("pass_field");
			String first_name = request.getParameter("first_name_field");
			String last_name = request.getParameter("last_name_field");
			String mail = request.getParameter("mail_field");
			String city = request.getParameter("city_field");
			String country = request.getParameter("country_field");
			String date = request.getParameter("date_field");
			String gender = request.getParameter("gender_field");

			if(username==null || password == null || username.equals("") || password.equals("")){
				msg = "Username or password missing";
				session.setAttribute("msg",msg);
				response.sendRedirect("pages/notification.jsp");
				return;
			}

			if(first_name==null || first_name.equals("")) first_name=null;
			if(last_name==null || last_name.equals("")) last_name=null;
			if(mail==null || mail.equals("")) mail=null;
			if(city==null || city.equals("")) city=null;
			if(country==null || country.equals("")) country=null;
			if(date==null || date.equals("")) date=null;
			if(gender==null || gender.equals("")) gender=null;
			
			Boolean sucess = f.register(first_name,last_name,username,mail,password,gender,city,country,date);

			if(!sucess){
				msg = "User "+username+" already exists! You will now be redirected to the main page...";
			}else{
				msg = "User "+username+" registered sucessfully! You will now be redirected to the main page...";
			}
			session.setAttribute("msg", msg);
			response.sendRedirect("pages/notification.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class ChangePassword extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			
			String new_password = request.getParameter("new_password_field");
			String old_password = request.getParameter("old_password_field");

			if(old_password==null || new_password==null || old_password.equals("") || new_password.equals("")){
				response.sendRedirect("/bd/pages/settings.jsp");
				return;
			}

			Boolean sucess = f.changePassword(old_password,new_password);
			System.out.println("Password change for user "+f.getUsername()+" : "+sucess);
			response.sendRedirect("/bd/pages/settings.jsp");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class DeleteInfo extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			
			Boolean sucess = f.deleteInfo();
			System.out.println("Delete all info from user "+f.getUsername()+" : "+sucess);

			response.sendRedirect("/bd/pages/settings.jsp");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
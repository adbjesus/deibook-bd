package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.MultipartConfig;

import upload.*;
import bean.Functions;

/*@MultipartConfig(location = "/tmp", maxFileSize = 10485760L) // 10MB.*/

public class AddPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			//MultipartMap map = new MultipartMap(request, this);
			String post = request.getParameter("post_field");
			//File file = map.getFile("post_image");
			String msg="";
			
			Boolean sucess = f.addPost(post,null);

			if(!sucess){
				msg = "Post not added!";
			}else{
				msg = "Post added!";
			}
			System.out.println(msg);
			response.sendRedirect("/bd/pages/posts.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}

}
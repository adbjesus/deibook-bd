package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class UpdateProfile extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			

			/*Update profile type*/
			String type_string = request.getParameter("profile_type");
			Boolean type = null;

			if(type_string == null){
				response.sendRedirect("/bd/pages/profile.jsp");
				return;
			}
			if(type_string.equals("false")){
				type = false;
			}
			else if(type_string.equals("true")){
				type = true;
			}
			else{
				response.sendRedirect("/bd/pages/profile.jsp");
				return;	
			}

			Boolean sucess = f.setProfileType(type);
			System.out.println("Set profile type to "+type+" sucess: "+sucess);

			/*Change other parameters*/
			String username = request.getParameter("username_field");
			String first_name = request.getParameter("first_name_field");
			String last_name = request.getParameter("last_name_field");
			String mail = request.getParameter("mail_field");
			String city = request.getParameter("city_field");
			String country = request.getParameter("country_field");
			String date = request.getParameter("date_field");
			String gender = request.getParameter("gender_field");

			if(username == null || username.equals("")){
				username = f.getUsername();				
			}

			if(first_name==null || first_name.equals("")) first_name=null;
			if(last_name==null || last_name.equals("")) last_name=null;
			if(mail==null || mail.equals("")) mail=null;
			if(city==null || city.equals("")) city=null;
			if(country==null || country.equals("")) country=null;
			if(date==null || date.equals("")) date=null;
			if(gender==null || gender.equals("")) gender=null;

			sucess = f.updateInfo(first_name,last_name,f.getUsername(),mail,gender,city,country,date);

			System.out.println("Update info sucess: "+sucess);

			response.sendRedirect("/bd/pages/profile.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
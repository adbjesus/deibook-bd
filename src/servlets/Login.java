package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class Login extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
	 		HttpSession session = request.getSession(true);
			
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}

			String password = request.getParameter("pass_field");
	 		String username = request.getParameter("username_field");

	 		Boolean login = f.login(username,password);			
			
	 		if(login){
	 			response.sendRedirect("pages/main.jsp");
	 		}else{
				String msg= "Wrong username/password! Please try again...";
	 			session.setAttribute("msg", msg);
	 			response.sendRedirect("pages/notification.jsp");
	 		}
		} catch (Exception e) {
			e.printStackTrace();
	 	}
	}
}
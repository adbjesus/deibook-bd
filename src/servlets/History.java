package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class History extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			
			String user = request.getParameter("user_field");

			if(user==null || user.equals("")){
				response.sendRedirect("/bd/pages/mensagens.jsp");
				return;
			}

			if(f.getHistory(user)==null){
				response.sendRedirect("/bd/pages/mensagens.jsp");
				return;
			}
			response.sendRedirect("/bd/pages/viewhistory.jsp");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
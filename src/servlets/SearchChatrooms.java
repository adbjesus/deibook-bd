package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class SearchChatrooms extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String name = request.getParameter("name_field");
			String theme = request.getParameter("theme_field");

			if(name.equals("")){name=null;}
			if(theme.equals("")){theme=null;}
			
			f.searchChatrooms(name,theme);
			response.sendRedirect("/bd/pages/search_result_chatrooms.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

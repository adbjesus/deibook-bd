package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class UpdateChatroom extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			
			String status = request.getParameter("status_field");
			Boolean type = null;

			if(status == null){
				response.sendRedirect("/bd/pages/list_chatrooms.jsp");
				return;
			}
			if(status.equals("false")){
				type = false;
			}
			else if(status.equals("true")){
				type = true;
			}
			else{
				response.sendRedirect("/bd/pages/list_chatrooms.jsp");
				return;	
			}

			Boolean sucess = f.updateChatroomStatus(f.getUsername(),f.getChatroom()[2],type);

			String theme = request.getParameter("theme_field");

			if(theme == null || theme.equals("")){
				theme = f.getChatroom()[2];				
			}

			sucess = f.updateChatroom(f.getUsername(),f.getChatroom()[2],theme);

			System.out.println("Update info sucess: "+sucess);

			response.sendRedirect("/bd/pages/list_chatrooms.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
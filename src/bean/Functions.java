package bean;

import java.util.*;
import java.io.*;
import java.nio.file.Files;
import javax.naming.*;
import javax.sql.*;
import java.sql.*;

public class Functions{

	private String username = null;
	private ArrayList<String[]> msgs = null;
	private ArrayList<String[]> chatrooms = null;
	private String[] chatroom;
	private int msgID;
	private String msgSender;
	private String msgReceiver;
	private String msgSendDate;
	private String msgReadDate;
	private String msgBody;
	private ArrayList<String> posts = null;

	private Connection connection = null;
	private Statement statement = null;
	private DataSource ds = null;
	private InitialContext cxt = null;

	public Functions(){
		this.username=null;
		try{
			cxt = new InitialContext();
			this.ds = (DataSource) cxt.lookup( "java:/comp/env/jdbc/postgres");
		} catch(Exception e){
			e.printStackTrace();
			return;
		}
	}	

	public Boolean prepareStatement(Boolean autocommit){
		try{
			this.connection = ds.getConnection();
			connection.setAutoCommit(autocommit);
			this.statement = connection.createStatement();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean commitConnection(){
		try{
			this.connection.commit();
			this.connection.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	} 

	public Boolean rollbackConnection(){
		try{
			this.connection.rollback();
			this.connection.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean closeConnection(){
		try{
			this.connection.close();
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean register(String first_name,String last_name,String username,String mail,String password,String gender,String city,String country,String date){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select registar(?,?,?,?,?,?,?,?,?)");
			pstmt.setString(1,first_name);
			pstmt.setString(2,last_name);
			pstmt.setString(3,username);
			pstmt.setString(4,mail);
			pstmt.setString(5,password);
			pstmt.setString(6,gender);
			pstmt.setString(7,city);
			pstmt.setString(8,country);
			if(date==null){
				pstmt.setDate(9,null);
			}
			else{
				pstmt.setDate(9,java.sql.Date.valueOf(date));
			}
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public Boolean login(String username,String password){
		//Database login
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select login(?,?)");
			pstmt.setString(1,username);
			pstmt.setString(2,password);
			pstmt.executeQuery();
			this.commitConnection();
			this.setUsername(username);
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			this.setUsername(null);
			return false;
		}		
		return true;
	}

	public Boolean deleteUser(){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select eliminarConta(?)");
			pstmt.setString(1,this.username);
			pstmt.executeQuery();
			this.commitConnection();
			this.setUsername(null);
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean setProfileType(Boolean type){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select mudarTipoPerfil(?,?)");
			pstmt.setString(1,this.username);
			pstmt.setBoolean(2,type);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean getProfileType(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_profile_type(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			Boolean ret = rs.getBoolean(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return false;
		}
	}

	public String getFirstName(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_name(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getLastName(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_surname(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getMail(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_email(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getAge(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_age(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getCity(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_city(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getCountry(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_country(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getDate(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_birthdate(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public String getGender(String username){
		try{
			this.prepareStatement(true);
			PreparedStatement pstmt = this.connection.prepareStatement("select get_user_gender(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			rs.next();
			String ret = rs.getString(1);
			this.closeConnection();
			return ret;
		} catch (Exception e){
			this.closeConnection();
			e.printStackTrace();
			return null;
		}
	}

	public Boolean updateInfo(String first_name,String last_name,String username,String mail,String gender,String city,String country,String date){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select actualizarInfo(?,?,?,?,?,?,?,?)");
			pstmt.setString(1,first_name);
			pstmt.setString(2,last_name);
			pstmt.setString(3,username);
			pstmt.setString(4,mail);
			pstmt.setString(5,gender);
			pstmt.setString(6,city);
			pstmt.setString(7,country);
			if(date==null){
				pstmt.setDate(8,null);
			}
			else{
				pstmt.setDate(8,java.sql.Date.valueOf(date));
			}
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public Boolean changePassword(String old_password,String new_password){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select alterarPassword(?,?,?)");
			pstmt.setString(1,this.username);
			pstmt.setString(2,old_password);
			pstmt.setString(3,new_password);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean deleteInfo(){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select apagarInformacao(?)");
			pstmt.setString(1,this.username);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean sendMessage(String receiver,String message,String date){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select enviarMensagemPessoal(?,?,?,?)");
			pstmt.setString(1,receiver);
			pstmt.setString(2,message);
			pstmt.setString(3,this.username);
			if(date!=null){
				pstmt.setTimestamp(4,Timestamp.valueOf(date));
			}
			else{
				pstmt.setTimestamp(4,null);	
			}
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ArrayList<String[]> getInbox(){
		try{
			ArrayList<String[]> msgs = new ArrayList<>();
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from listMensagensRecebidas(?)");
			pstmt.setString(1,this.username);
			ResultSet rs = pstmt.executeQuery();
			String[] tmp;
			while(rs.next()){
				tmp = new String[6];
				tmp[0] = rs.getString("id");
				tmp[1] = rs.getString("from");
				tmp[2] = rs.getString("to");
				tmp[3] = rs.getString("body");
				tmp[4] = rs.getString("send_date");
				tmp[5] = rs.getString("read_date");
				msgs.add(tmp);
			}
			this.commitConnection();
			this.msgs = msgs;
			return msgs;
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<String[]> getOutbox(){
		try{
			ArrayList<String[]> msgs = new ArrayList<>();
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from listMensagensEnviadas(?)");
			pstmt.setString(1,this.username);
			ResultSet rs = pstmt.executeQuery();
			String[] tmp;
			while(rs.next()){
				tmp = new String[6];
				tmp[0] = rs.getString("id");
				tmp[1] = rs.getString("from");
				tmp[2] = rs.getString("to");
				tmp[3] = rs.getString("body");
				tmp[4] = rs.getString("send_date");
				tmp[5] = rs.getString("read_date");
				msgs.add(tmp);
			}
			this.commitConnection();
			this.msgs = msgs;
			return msgs;
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<String[]> getHistory(String username){
		try{
			ArrayList<String[]> msgs = new ArrayList<>();
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from listConversa(?,?)");
			pstmt.setString(1,this.username);
			pstmt.setString(2,username);
			ResultSet rs = pstmt.executeQuery();
			String[] tmp;
			while(rs.next()){
				tmp = new String[6];
				tmp[0] = rs.getString("id");
				tmp[1] = rs.getString("from");
				tmp[2] = rs.getString("to");
				tmp[3] = rs.getString("body");
				tmp[4] = rs.getString("send_date");
				tmp[5] = rs.getString("read_date");
				msgs.add(tmp);
			}
			this.commitConnection();
			this.msgs = msgs;
			return msgs;
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<String[]> getMsgs(){
		return this.msgs;
	}

	public Boolean deleteMessage(int msgID){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from apagarMensagem(?,?)");
			pstmt.setInt(1,msgID);
			pstmt.setString(2,this.username);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean createChatroom(String theme){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from criarSala(?,?)");
			pstmt.setString(1,this.username);
			pstmt.setString(2,theme);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public ArrayList<String[]> listChatrooms(String username){
		try{
			ArrayList<String[]> chatrooms = new ArrayList<>();
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from listarSalas(?)");
			pstmt.setString(1,username);
			ResultSet rs = pstmt.executeQuery();
			String[] tmp;
			while(rs.next()){
				tmp = new String[4];
				tmp[0] = rs.getString("chatroom_id");
				tmp[1] = rs.getString("username");
				tmp[2] = rs.getString("theme");
				tmp[3] = rs.getString("active");
				chatrooms.add(tmp);
			}
			this.commitConnection();
			this.chatrooms = chatrooms;
			return chatrooms;
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return null;
		}
	}

	public Boolean updateChatroomStatus(String username, String theme, Boolean status){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from mudarstatussala(?,?,?)");
			pstmt.setString(1,username);
			pstmt.setString(2,theme);
			pstmt.setBoolean(3,status);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public Boolean updateChatroom(String username, String old_theme,String new_theme){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from modificarSala(?,?,?)");
			pstmt.setString(1,username);
			pstmt.setString(2,old_theme);
			pstmt.setString(3,new_theme);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public Boolean givePermission(int id, String username,Boolean permission){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from criapermissao(?,?,?)");
			pstmt.setInt(1,id);
			pstmt.setString(2,username);
			pstmt.setBoolean(3,permission);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public ArrayList<String[]> searchChatrooms(String username,String theme){
		try{
			ArrayList<String[]> chatrooms = new ArrayList<>();
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from listarSalasporusertheme(?,?)");
			pstmt.setString(1,username);
			pstmt.setString(2,theme);
			ResultSet rs = pstmt.executeQuery();
			String[] tmp;
			while(rs.next()){
				tmp = new String[4];
				tmp[0] = rs.getString("chatroom_id");
				tmp[1] = rs.getString("username");
				tmp[2] = rs.getString("theme");
				tmp[3] = rs.getString("active");
				chatrooms.add(tmp);
			}
			this.commitConnection();
			this.chatrooms = chatrooms;
			return chatrooms;
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return null;
		}
	}

	public Boolean readPosts(int id){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from lerpost(?)");
			pstmt.setInt(1,id);
			ResultSet rs = pstmt.executeQuery();
			this.posts = new ArrayList<>();
			while(rs.next()){
				this.posts.add(rs.getString("body"));
			}
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}	
		return true;
	}

	public String getUsername(){
		return this.username;
	}

	public ArrayList<String[]> getChatrooms(){
		return this.chatrooms;
	}

	public void setChatroom(int id){
		for(int i=0;i<this.chatrooms.size();i++){
			if(Integer.parseInt(this.chatrooms.get(i)[0])==id){
				this.chatroom = chatrooms.get(i);
				return;
			}
		}
		return;
	}

	public String[] getChatroom(){

		return this.chatroom;
	}

	public void setUsername(String username){
		this.username = username;
	}
	
	public void setMsgID(int msgID){
		this.msgID = msgID;
	}

	public int getMsgID(){
		return this.msgID;
	}
	
	public Boolean logout() {
		Boolean ret=false;
		//Database logout		
		return false;
	}
	
	public String[] getMsg(int i){
		return this.msgs.get(i);
	}

	public String getMsgSender(){
		this.msgSender = getMsg(this.msgID)[1];
		return this.msgSender;
	}

	public String getMsgReceiver(){
		this.msgReceiver = getMsg(this.msgID)[2];
		return this.msgReceiver;
	}

	public String getMsgSendDate(){
		this.msgSendDate = getMsg(this.msgID)[4];
		return this.msgSendDate;
	}

	public String getMsgReadDate(){
		this.msgReadDate = getMsg(this.msgID)[5];
		return this.msgReadDate;
	}

	public String getMsgBody(){
		// DB get message body
		// this.msgBody = getMsg(this.msgID).getMessage();
		this.msgBody = getMsg(this.msgID)[3];
		return this.msgBody;
	}

	public String getMsgRealID(){
		return getMsg(this.msgID)[0];
	}

	public ArrayList<String> getPosts(){
		return this.posts;
	}

	public Boolean addPost(String post, File file){
		try{
			this.prepareStatement(false);
			PreparedStatement pstmt = this.connection.prepareStatement("select * from criarPost(?,?,?,?)");
			pstmt.setInt(1,Integer.parseInt(this.chatroom[0]));
			//pstmt.setInt(1,1);
			pstmt.setString(2,this.username);
			pstmt.setString(3,post);
			pstmt.setDate(4,null);
			pstmt.executeQuery();
			this.commitConnection();
		} catch (Exception e){
			this.rollbackConnection();
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Boolean deletePost(int id){
		// Delete a post from db
		return false;
	}
}
